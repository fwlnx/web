![fwlnx](https://gitlab.com/fwlnx/fwlnx/-/raw/master/fwlnx.png)

# fwlnx

Firewall Linux - A small and simple Linux based Firewall Distribution

It combines a smart Linux distribution ([NixOS](https://nixos.org/)) with modern network services, a simple web interface and an API to configure it. It's not meant to be a replacement for far superiour Appliances like [pfSense](https://www.pfsense.org/) or [OPNSense](https://opnsense.org/) but as an alternative solution for typical home use. Think FRITZ!Box on steroids.

## Target platforms

- QEMU/KVM
- [PCEngines APU2/3/4](https://www.pcengines.ch/apu2.htm)
- Generic [AliExpress](https://www.aliexpress.com/wholesale?catId=0&SearchText=router+pc) Router PCs

## Planned initial features

- Firewall ([netfilter](https://netfilter.org/))
  - [ ] NAT
  - [ ] Rules
  - [ ] UPnP ([miniupnp](https://miniupnp.tuxfamily.org/))
- [ ] DynDNS ([ddclient](https://ddclient.net/))
- VPN
  - [ ] [OpenVPN](https://openvpn.net/)
  - [ ] [Wireguard](https://www.wireguard.com/)
- [ ] NTP ([chrony](https://chrony.tuxfamily.org/))
- [ ] DHCP ([Kea](https://www.isc.org/kea/))
- [ ] DNS ([Knot](https://www.knot-dns.cz/))
- [ ] mDNS/DNS-SD ([Avahi](https://www.avahi.org/))
- [ ] TFTP ([Netkit TFTP](http://ftp.linux.org.uk/pub/linux/Networking/netkit/))
- [ ] Auto config backup
- [ ] Cron/Timers ([Systemd-timers](https://www.freedesktop.org/software/systemd/man/systemd.timer.html))


## Minimum hardware requirements

- x86_64 Multicore CPU
- 4GB RAM
- 64GB storage
- 2x ethernet ports
  - one dedicated WAN port
  - one dedicated LAN port

## Installation

TBD

## Design

### Partitioning

The partitioning schema is kept simple and will be setup like this:

BIOS/MBR:

```
/dev/sda:
  /dev/sda1  swap  [SWAP]  4G        # SWAP
  /dev/sda2  ext4  /       100%FREE  # FWLNX
```

UEFI/GPT:

```
/dev/sda:
  /dev/sda1  vfat  /boot   1G        # ESP
  /dev/sda2  swap  [SWAP]  4G        # SWAP
  /dev/sda3  ext4  /       100%FREE  # FWLNX
```

Minimum size for the storage is 64GB. eMMC, SATA or NVMe SSDs should be preferred to SD cards. Minimum RAM size is 4GB, but more RAM is more good. 😉


### Operating system

As the base operating system NixOS is used. The backend daemon Garmr feeds configuration into nix expressions to configure the whole system.

### [Garmr](https://gitlab.com/fwlnx/garmr)

Backend daemon which exposes all configuration options via a HTTP API.

### [Freyja](https://gitlab.com/fwlnx/freyja)

Web frontend that hooks into Garmrs API to make it easier to configure fwlnx.


---

Made with ❤️ and 🪄✨.

Icon partly from [stockio](https://www.stockio.com/free-icon/nature-icons-penguin)
